# Yubikey-Analogues

| name         | FIDO | FIDO2 | OTP | GPG | static password | static password manager | PIV | Challenge-response | version with nfc | can use self FW      | opensource |
|--------------|------|-------|-----|-----|-----------------|-------------------------|-----|--------------------|------------------|----------------------|------------|
| Yubikey 5    | +    | +     | +   | +   | +               | -                       | +   | +                  | +                | -                    | -          |
| Nitrokey     | +    | +     | +   | +   |                 | +?                      | +   | -                  | -                | -                    | +          |
| Thetis Fido  | +    | -     | -   | -   | -               | -                       | -   | -                  | -                | -                    | -          |
| Trezor       | +    | +     | -   | -   | -               | -                       | -   | -                  | -                | -                    | -          |
| ledger       | +    | -     | -   | +   | -               | -                       | -   | -                  | -                | -                    | -          |
| Tomu         | +    | +     | ?   | ?   | ?               |                         | ?   | ?                  | -                | +                    | +          |
| OnlyKey      | +    | +     | +   | -   | +?              | +?                      | -   | +                  | -                | -                    | -          |
| Librem Key   | -    | -     | +   | +   | +?              | +?                      | -   | -                  | -                | -                    | +?         |
| U2F Zero     | +    | -     | -   | -   | -               | -                       | -   | -                  | -                | +                    | +          |
| GOOGLE TITAN | +    | -     | -   | -   | -               | -                       | -   | -                  | -                | -                    | -          |
| HyperFIDO    | +    | +     | -   | -   | -               | -                       | -   | -                  | -                | -                    | -          |
| OpenSK       | +    | +     | -   | -   | -               | -                       | -   | -                  | -                | +                    | +          |
| solokey      | +    | +     | -   | -   | -               | -                       | -   | -                  | +                | +(on unlock wersion) | +          |
| solokey 2    | +    | +     | +   | +?  | ?               | ?                       | +   | ?                  | +                | +(on unlock wersion) | +          |
